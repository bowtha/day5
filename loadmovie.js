function loadAllMovie(){
    var responseData = null;
    var data=fetch('https://dv-excercise-backend.appspot.com/movies')
        .then((response)=>{
            console.log(response)
            return response.json()
        })
        .then((json)=>{
            responseData = json
            var resultElement = document.getElementById('result')
            resultElement.innerHTML = JSON.stringify(json, null, 2)
        })
    // console.log(data)
}

async function loadAllMovieAsync(){
    let response = await fetch('https://dv-excercise-backend.appspot.com/movies')
    let data = await response.json()
    // var resultElement =document.getElementById('result')
    // resultElement.innerHTML = JSON.stringify(json, null, 2)
    return data
}

function createResultTable(data){
    let resultElement = document.getElementById('resultTable')
    let tableNode = document.createElement('table')
    resultElement.appendChild(tableNode)
    tableNode.setAttribute('class','table')

    let tableHeadNode = document.createElement('thead')
    tableNode.appendChild(tableHeadNode)
    var tableRowNode = document.createElement('tr')
    tableNode.appendChild(tableRowNode)

    var tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope','col')
    tableHeaderNode.innerText = 'Movie Name'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope','col')
    tableHeaderNode.innerText = 'Synopsis'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope','col')
    tableHeaderNode.innerText = 'Image'
    tableHeadNode.appendChild(tableHeaderNode)

    console.log(data)

    data.then((json) => {
        for (let i=0 ; i<json.length;i++){
            var currentData = json[i]
            var dataRow = document.createElement('tr')
            tableNode.appendChild(dataRow)
            var dataFirstColumnNode = document.createElement('th')
            dataFirstColumnNode.setAttribute('scope','row')
            dataFirstColumnNode.innerText = currentData['name']
            dataRow.appendChild(dataFirstColumnNode)

            var columnNode = null;

            columnNode = document.createElement('td')
            columnNode.innerText = currentData['synopsis']
            dataRow.appendChild(columnNode)

            columnNode = document.createElement('td')
            var imageNode = document.createElement('img')
            imageNode.setAttribute('src',currentData['imageUrl'])
            imageNode.style.width = '100px'
            imageNode.style.height = '100px'
            dataRow.appendChild(imageNode)
        }
    })
}

async function loadOneMovieAsync(){
    let MovieName = document.getElementById('queryName').value
    let response = await fetch('https://dv-excercise-backend.appspot.com/movies/'+MovieName)
    let data = await response.json()
    return data
}