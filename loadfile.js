function loadAllStudents(){
    var responseData = null;
    var data=fetch('http://dv-student-backend-2019.appspot.com/students')
        .then((response)=>{
            console.log(response)
            return response.json()
        })
        .then((json)=>{
            responseData = json
            var resultElement = document.getElementById('result')
            resultElement.innerHTML = JSON.stringify(json, null, 2)
        })
}

async function loadAllStudentAsync(){
    let response = await fetch('http://dv-student-backend-2019.appspot.com/students')
    let data = await response.json()
    // var resultElement =document.getElementById('result')
    // resultElement.innerHTML = JSON.stringify(json, null, 2)
    return data
}

function createResultTable(data){
    let resultElement = document.getElementById('resultTable')
    let tableNode = document.createElement('table')
    resultElement.appendChild(tableNode)
    tableNode.setAttribute('class','table')

    let tableHeadNode = document.createElement('thead')
    tableNode.appendChild(tableHeadNode)
    var tableRowNode = document.createElement('tr')
    tableNode.appendChild(tableRowNode)

    var tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope','col')
    tableHeaderNode.innerText = '#'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope','col')
    tableHeaderNode.innerText = 'StudentId'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope','col')
    tableHeaderNode.innerText = 'Name'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope','col')
    tableHeaderNode.innerText = 'Surname'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope','col')
    tableHeaderNode.innerText = 'GPA'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope','col')
    tableHeaderNode.innerText = 'Image'
    tableHeadNode.appendChild(tableHeaderNode)

    console.log(data)

    data.then((json) => {
        for (let i=0 ; i<json.length;i++){
            var currentData = json[i]
            var dataRow = document.createElement('tr')
            tableNode.appendChild(dataRow)
            var dataFirstColumnNode = document.createElement('th')
            dataFirstColumnNode.setAttribute('scope','row')
            dataFirstColumnNode.innerText = currentData['id']
            dataRow.appendChild(dataFirstColumnNode)

            var columnNode = null;
            columnNode = document.createElement('td')
            columnNode.innerText = currentData['studentId']
            dataRow.appendChild(columnNode)

            columnNode = document.createElement('td')
            columnNode.innerText = currentData['name']
            dataRow.appendChild(columnNode)

            columnNode = document.createElement('td')
            columnNode.innerText = currentData['surname']
            dataRow.appendChild(columnNode)

            columnNode = document.createElement('td')
            columnNode.innerText = currentData['gpa']
            dataRow.appendChild(columnNode)

            columnNode = document.createElement('td')
            var imageNode = document.createElement('img')
            imageNode.setAttribute('src',currentData['image'])
            imageNode.style.width = '100px'
            imageNode.style.height = '100px'
            dataRow.appendChild(imageNode)
        }
    })
}

async function loadOneStudent(data){
    let studentId = document.getElementById('queryId').value
    if (studentId != '' && studentId != null){

        let resultElement = document.getElementById('resultTable')
        let tableNode = document.createElement('table')
        resultElement.appendChild(tableNode)
        tableNode.setAttribute('class','table')

        let tableHeadNode = document.createElement('thead')
        tableNode.appendChild(tableHeadNode)
        var tableRowNode = document.createElement('tr')
        tableNode.appendChild(tableRowNode)

        var tableHeaderNode = document.createElement('th')
        tableHeaderNode.setAttribute('scope','col')
        tableHeaderNode.innerText = '#'
        tableHeadNode.appendChild(tableHeaderNode)

        tableHeaderNode = document.createElement('th')
        tableHeaderNode.setAttribute('scope','col')
        tableHeaderNode.innerText = 'StudentId'
        tableHeadNode.appendChild(tableHeaderNode)

        tableHeaderNode = document.createElement('th')
        tableHeaderNode.setAttribute('scope','col')
        tableHeaderNode.innerText = 'Name'
        tableHeadNode.appendChild(tableHeaderNode)

        tableHeaderNode = document.createElement('th')
        tableHeaderNode.setAttribute('scope','col')
        tableHeaderNode.innerText = 'Surname'
        tableHeadNode.appendChild(tableHeaderNode)

        tableHeaderNode = document.createElement('th')
        tableHeaderNode.setAttribute('scope','col')
        tableHeaderNode.innerText = 'GPA'
        tableHeadNode.appendChild(tableHeaderNode)

        tableHeaderNode = document.createElement('th')
        tableHeaderNode.setAttribute('scope','col')
        tableHeaderNode.innerText = 'Image'
        tableHeadNode.appendChild(tableHeaderNode)

        console.log(data)

        data.then((obj) => {
                var currentData = obj
                var dataRow = document.createElement('tr')
                tableNode.appendChild(dataRow)
                var dataFirstColumnNode = document.createElement('th')
                dataFirstColumnNode.setAttribute('scope','row')
                dataFirstColumnNode.innerText = currentData['id']
                dataRow.appendChild(dataFirstColumnNode)

                var columnNode = null;
                columnNode = document.createElement('td')
                columnNode.innerText = currentData['studentId']
                dataRow.appendChild(columnNode)

                columnNode = document.createElement('td')
                columnNode.innerText = currentData['name']
                dataRow.appendChild(columnNode)

                columnNode = document.createElement('td')
                columnNode.innerText = currentData['surname']
                dataRow.appendChild(columnNode)

                columnNode = document.createElement('td')
                columnNode.innerText = currentData['gpa']
                dataRow.appendChild(columnNode)

                columnNode = document.createElement('td')
                var imageNode = document.createElement('img')
                imageNode.setAttribute('src',currentData['image'])
                imageNode.style.width = '100px'
                imageNode.style.height = '100px'
                dataRow.appendChild(imageNode)
            })
    }
}

async function loadOneStudentAsync(){
    let studentId = document.getElementById('queryId').value
    let response = await fetch('http://dv-student-backend-2019.appspot.com/students/'+studentId)
    let data = await response.json()

    return data
}